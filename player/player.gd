extends Area2D

var scene_size
var extents
var vel = Vector2(400,0)

func _ready():
    scene_size = get_viewport().get_visible_rect().size
    extents = $texture.texture.get_size() / 2

func _process(delta):
    var d = delta*vel.x
    position.x += d if Input.is_action_pressed("ui_right")  else \
                (-d if Input.is_action_pressed("ui_left") else 0)
    position.x = clamp(position.x, extents.x, scene_size.x - extents.x)

# vim:set et sw=4 ts=4 tw=120:

