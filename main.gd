extends Node

onready var ballscn   = preload("res://ball/ball.tscn")
onready var playerscn = preload("res://player/player.tscn")

var theball
var theplayer

func _ready():
    theball = ballscn.instance()
    add_child(theball)
    theplayer = playerscn.instance()
    add_child(theplayer)

func _process(delta):
    var ballplayerdir = theball.position + Vector2(0, theball.extents.y) \
                        - (theplayer.position + Vector2(0, theplayer.extents.y))
    if Vector2(0,-1).dot(ballplayerdir) < 0:
        print("Player lost!")

# vim:set et sw=4 ts=4 tw=120:

