# INF5071 - Labo 05 - Exercice Godot

Le laboratoire a pour but de démontrer l'utilisation des concepts de
[vecteurs][] et [matrices][] dans Godot.

[vecteurs]: http://docs.godotengine.org/en/stable/tutorials/math/vector_math.html#doc-vector-math
[matrices]: http://docs.godotengine.org/en/stable/tutorials/math/matrices_and_transforms.html#doc-matrices-and-transforms

## Projet Godot

Le dépôt contient les fichiers rédigés durant le laboratoire. On y retrouve:

- `project.godot`: Fichier contenant les configurations générales du projet;
- `*.tscn`: les fichiers décrivant la configuration des scènes;
- `*.gd`: les scripts (écrits en `godotscript`) utilisés dans les scènes;

D'autres fichiers comme des images sont aussi versionnés dans le dépôt. Pour de
l'information sur comment structurer un dépôt, voyez les liens suivants:

- **Méthodologie**: Le sujet de versionnage du code est abordé en plus de
  plusieurs conseils sur l'organisation d'un projet godot.

      http://docs.godotengine.org/en/3.0/getting_started/workflow/index.html

- **Projet C#**: Pour configurer un dépôt C#. Des détails sur le versionnage
  sont présents.

      http://docs.godotengine.org/en/3.0/getting_started/scripting/c_sharp/c_sharp_basics.html

- **Importation de ressources**: Guide sur la manière d'importer des ressources
  et l'effet que ça a sur le projet et ses fichiers. Le sujet du versionnage est
  aussi couvert.

      http://docs.godotengine.org/en/3.0/getting_started/workflow/assets/import_process.html?highlight=version%20control

### `.gitignore`

Le fichier `.gitignore` contient des motifs à ignorer dans le dépôt. L'attention
du lecteur devrait être amenée vers le motif `.import`. Godot génère des
fichiers automatiquement dont un répertoire `.import` à la racine, et des
fichiers correspondants à tous les fichiers ressources comme des images. Par
exemple, dans le dépôt, on a les fichiers `bar.png.import` et `icon.png.import`.
Ces fichiers [doivent être versionnés][import] contrairement au répertoire
`.import` qui peut être ignoré. *Je recommande de l'ignorer*.

[import]: http://docs.godotengine.org/en/3.0/getting_started/workflow/assets/import_process.html#files-generated

<!-- vim: set ts=4 sw=4 tw=80 et :-->

