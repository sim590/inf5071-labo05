extends Area2D

var vel = Vector2(100,100)
var scene_size
var extents

func _ready():
    scene_size = get_viewport().get_visible_rect().size
    extents = $texture.texture.get_size() / 2

func _process(delta):
    rotate(delta * PI/4)
    var bot_right_border_pos = position + extents
    var top_left_border_pos = position - extents
    if bot_right_border_pos.x >= scene_size.x or top_left_border_pos.x <= 0:
        vel.x *= -1
    if bot_right_border_pos.y >= scene_size.y or top_left_border_pos.y <= 0:
        vel.y *= -1
    position += delta * vel

# vim:set et sw=4 ts=4 tw=120:



func _on_ball_area_entered( area ):
    if area.name == "player":
        vel.y *= -1
